# Hacker News Aggregator (Hna)

## Reasoning for implementation

I've decided to use `:ets`, with `read_concurrency: true` to store the latest 
stories. The main reasoning for this is the fact that, writes are going to be
done by a single process and reads can be done anytime by multiple processes,
so we don't have blocking operations on the read side.

This `:ets` is started together with the Hacker News client' Supervisor, so in 
case the Hacker News Client **(HNC)** dies, the data stored is not lost.

The **HNC** is responsible for updating `:ets`, pushing new data to clients via
`Hna.Endpoint.broadcast` subscribed to the given channel `hacker_news:lobby`.

Since there are, at least, 51 calls required to be made to **HNC** API, the 
first one to get the IDs of the stories and the other 50 ones to get the story
itself, I've decided to do these 50 concurrently, using `Task.async_stream`,
which would improve performance.

## Points of improvement & considerations

- Testing **HNC**: I haven't implemented testing on the client due to the
  fact that it may need to use mocks and a clever approach on checking
  messages received via instrumentation.
- All important parts of the system are properly named (processes and `ets`
  tables), so it's clearer when using `observer`.
- I've kept dependencies to a minimum. Only extra depedency I've added is
  `httpoison`.