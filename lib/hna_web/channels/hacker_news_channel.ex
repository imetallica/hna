defmodule HnaWeb.HackerNewsChannel do
  use Phoenix.Channel

  @amount_to_send 50

  def join("hacker_news:lobby", _message, socket) do
    send(self(), :after_join)
    {:ok, socket}
  end

  def handle_info(:after_join, socket) do
    news = Hna.HackerNews.interval(0, @amount_to_send)
    push(socket, "joined", %{"data" => news})
    {:noreply, socket}
  end
end
