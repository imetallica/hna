defmodule HnaWeb.V1.StoriesView do
  use HnaWeb, :view

  def render("index.json", %{stories: stories}) do
    %{data: render_many(stories, __MODULE__, "story.json", as: :story)}
  end

  def render("show.json", %{story: story}) do
    %{data: render_one(story, __MODULE__, "story.json", as: :story)}
  end

  def render("story.json", %{story: story}) do
    story
  end
end
