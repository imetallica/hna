defmodule HnaWeb.V1.StoriesController do
  use HnaWeb, :controller

  @doc """
  List all stories. Pagination starts at 0.
  """
  @spec index(Plug.Conn.t(), map) :: Plug.Conn.t()
  def index(conn, %{"page" => page}) do
    with {number, _rest} <- Integer.parse(page),
         stories = Hna.HackerNews.interval(number * 10) do
      render(conn, "index.json", %{stories: stories})
    else
      _ ->
        conn
        |> put_status(:bad_request)
        |> put_view(HnaWeb.ErrorView)
        |> render("400.json")
    end
  end

  def index(conn, _params) do
    stories = Hna.HackerNews.interval(0)
    render(conn, "index.json", %{stories: stories})
  end

  @doc """
  Returns a story with given id.
  """
  def show(conn, %{"id" => id}) do
    with {id, _rest} <- Integer.parse(id),
         story = Hna.HackerNews.fetch(id) do
      render(conn, "show.json", %{story: story})
    else
      _ ->
        conn
        |> put_status(:bad_request)
        |> put_view(HnaWeb.ErrorView)
        |> render("400.json")
    end
  end

  def show(conn, _params) do
    conn
    |> put_status(:bad_request)
    |> put_view(HnaWeb.ErrorView)
    |> render("400.json")
  end
end
