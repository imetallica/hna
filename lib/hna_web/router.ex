defmodule HnaWeb.Router do
  use HnaWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api/v1", HnaWeb.V1, as: :api_v1 do
    pipe_through :api

    get "/stories", StoriesController, :index
    get "/stories/:id", StoriesController, :show
  end
end
