defmodule Hna.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the endpoint when the application starts
      HnaWeb.Endpoint,
      # Starts a worker by calling: Hna.Worker.start_link(arg)
      # {Hna.Worker, arg},

      %{
        id: Hna.HackerNews,
        start: {Hna.HackerNews, :start_link, [[], [name: Hna.HackerNews.Supervisor]]}
      }
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Hna.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    HnaWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
