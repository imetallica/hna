defmodule Hna.HackerNews do
  use Supervisor

  @ets_table_name __MODULE__.ETS

  @doc """
  Inserts the current data into ETS, using "id" as the id of the table.
  If there is already a story, skips.
  """
  @spec insert(payload :: map) :: {:ok, map} | {:error, :already_set}
  def insert(%{"id" => id} = payload) do
    if :ets.insert_new(@ets_table_name, {id, payload}),
      do: {:ok, payload},
      else: {:error, :already_set}
  end

  @doc """
  Fetches a single record, since the ETS table is a bag.
  """
  @spec fetch(id :: non_neg_integer) :: nil | map()
  def fetch(id) when is_integer(id) and id > 0 do
    case :ets.lookup(@ets_table_name, id) do
      [] -> nil
      [{^id, elem}] -> elem
    end
  end

  @doc """
  Returns elements from ETS in an interval given.
  """
  @spec interval(from :: non_neg_integer, amount :: non_neg_integer) :: list(map)
  def interval(from, amount \\ 10)
      when is_integer(from) and is_integer(amount) and from >= 0 and amount > 0 do
    @ets_table_name
    |> :ets.tab2list()
    |> Enum.slice(from, amount)
    |> Enum.map(fn {_id, data} -> data end)
  end

  @spec start_link(args :: list, opts :: list) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(args, opts \\ []) do
    Supervisor.start_link(__MODULE__, args, opts)
  end

  @impl true
  def init(_opts) do
    :ets.new(@ets_table_name, [:set, :public, :named_table, read_concurrency: true])

    children = [
      %{
        id: Hna.HackerNews.Client,
        start: {Hna.HackerNews.Client, :start_link, [[], [name: Hna.HackerNews.Client]]}
      }
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
