defmodule Hna.HackerNews.Client do
  use GenServer
  alias Hna.HackerNews

  require Logger

  defstruct pool_ref: nil

  @type t :: %__MODULE__{pool_ref: reference()}

  @spec start_link(args :: list(), opts :: list()) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(args, opts \\ []) do
    GenServer.start_link(__MODULE__, args, opts)
  end

  @impl true
  @spec init(args :: list()) :: {:ok, t}
  def init(_args) do
    ref = do_schedule_pool(5_000)
    {:ok, %__MODULE__{pool_ref: ref}}
  end

  @impl true
  def handle_info(:refresh, %__MODULE__{} = state) do
    ref = do_schedule_pool()
    state = put_in(state.pool_ref, ref)

    with {:ok, ids} <- do_fetch_up_to_50_best_story_ids(),
         stories = do_fetch_stories(ids),
         stored_stories = do_store_stories(stories),
         :ok <- do_broadcast_stories(stored_stories) do
      {:noreply, state}
    else
      _ ->
        {:noreply, state}
    end
  end

  @spec do_broadcast_stories(stories :: list(map)) :: :ok | {:error, term}
  defp do_broadcast_stories(stories) when is_list(stories) do
    HnaWeb.Endpoint.broadcast("hacker_news:lobby", "updates", %{"data" => stories})
  end

  @spec do_store_stories(stories :: list(map)) :: list(map)
  defp do_store_stories(stories) when is_list(stories) do
    stories
    |> Enum.map(&do_store_story(&1))
    |> Enum.filter(&(&1 != :skipped))
  end

  @spec do_store_story(story :: map) :: map | :skipped
  defp do_store_story(story) when is_map(story) do
    case HackerNews.insert(story) do
      {:ok, story} -> story
      {:error, _} -> :skipped
    end
  end

  @spec do_fetch_stories(ids :: list(non_neg_integer)) :: list(map())
  defp do_fetch_stories(ids) when is_list(ids) do
    ids
    |> Task.async_stream(&do_fetch_item_by_id(&1))
    |> Enum.reduce([], fn
      {:ok, {:ok, story}}, acc -> acc ++ [story]
      _, acc -> acc
    end)
  end

  @spec do_fetch_item_by_id(id :: non_neg_integer) :: {:ok, map()} | {:error, term}
  defp do_fetch_item_by_id(id) when is_integer(id) and id > 0 do
    with {:ok, payload} <- do_request("https://hacker-news.firebaseio.com/v0/item/#{id}.json") do
      do_decode_payload(payload)
    end
  end

  @spec do_fetch_up_to_50_best_story_ids :: {:ok, list(non_neg_integer)} | {:error, term}
  defp do_fetch_up_to_50_best_story_ids do
    with {:ok, payload} <- do_request("https://hacker-news.firebaseio.com/v0/topstories.json"),
         {:ok, decoded_ids} <- do_decode_payload(payload) do
      {:ok, Enum.take(decoded_ids, 50)}
    end
  end

  @spec do_decode_payload(body :: iodata) :: {:ok, term} | {:error, term}
  defp do_decode_payload(body) do
    with {:error, %Jason.DecodeError{} = reason} <- Jason.decode(body) do
      Logger.error(fn -> "Invalid data from payload, #{inspect(reason)}." end)
      {:error, :decode_error}
    end
  end

  @spec do_request(path :: String.t()) :: {:ok, String.t()} | {:error, term}
  defp do_request(path) when is_binary(path) do
    with {:ok, %HTTPoison.Response{status_code: 200, body: body}} <-
           HTTPoison.get(path) do
      {:ok, body}
    else
      {:ok, %HTTPoison.Response{status_code: status_code, body: body}} ->
        Logger.warn(fn -> "Invalid status code expected: #{status_code}. Body is: #{body}." end)
        {:error, :invalid_response}

      {:error, %HTTPoison.Error{reason: reason}} ->
        Logger.error(fn ->
          "Something went wrong while requesting #{path}. Reason: #{inspect(reason)}."
        end)

        {:error, reason}
    end
  end

  @spec do_schedule_pool(timeout :: non_neg_integer) :: reference()
  defp do_schedule_pool(timeout \\ 300_000) when is_integer(timeout) and timeout > 0 do
    Logger.debug("Scheduling...")
    Process.send_after(__MODULE__, :refresh, timeout)
  end
end
