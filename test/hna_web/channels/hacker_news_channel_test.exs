defmodule HnaWeb.Channels.HackerNewsChannelTest do
  use HnaWeb.ChannelCase

  alias HnaWeb.Fixtures.StoriesFixture

  describe "that the websocket" do
    setup do
      Enum.each(StoriesFixture.stories(), fn story ->
        :ets.insert(Hna.HackerNews.ETS, {story["id"], story})
      end)

      {:ok, %{socket: socket(HnaWeb.UserSocket)}}
    end

    test "receives the top 50 stories upon connection.", %{socket: socket} do
      data = Hna.HackerNews.interval(0, 50)
      {:ok, _, socket} = subscribe_and_join(socket, "hacker_news:lobby")

      assert_push("joined", %{"data" => ^data})
    end

    test "receives updates when new stories arrive.", %{socket: socket} do
      HnaWeb.Endpoint.subscribe("hacker_news:lobby")

      assert_receive %{payload: %{"data" => data}}, 10_000
    end
  end
end
