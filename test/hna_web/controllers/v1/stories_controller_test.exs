defmodule HnaWeb.Controllers.V1.StoriesControllerTest do
  use HnaWeb.ConnCase

  alias HnaWeb.Fixtures.StoriesFixture

  describe "/api/v1" do
    setup %{conn: conn} do
      Enum.each(StoriesFixture.stories(), fn story ->
        :ets.insert(Hna.HackerNews.ETS, {story["id"], story})
      end)

      :ets.insert(Hna.HackerNews.ETS, {123, %{"foo" => 123, "id" => 123}})

      {:ok, %{conn: conn}}
    end

    test "/stories returns a list of top stories from hacker news.", %{conn: conn} do
      result = conn |> get(Routes.api_v1_stories_path(conn, :index)) |> json_response(200)

      assert is_list(result["data"])
    end

    test "/stories?page=0 and /stories behave the same.", %{conn: conn} do
      result1 = conn |> get(Routes.api_v1_stories_path(conn, :index)) |> json_response(200)

      result2 =
        conn
        |> get(Routes.api_v1_stories_path(conn, :index), %{"page" => "0"})
        |> json_response(200)

      assert result1 == result2
    end

    test "/stories?page=0 and /stories?page=1 return different results.", %{conn: conn} do
      result1 =
        conn
        |> get(Routes.api_v1_stories_path(conn, :index), %{"page" => "0"})
        |> json_response(200)

      result2 =
        conn
        |> get(Routes.api_v1_stories_path(conn, :index), %{"page" => "1"})
        |> json_response(200)

      refute result1 == result2
    end

    test "/stories?page=X when X is bigger than the amount of stored stories returns an empty list.",
         %{conn: conn} do
      result =
        conn
        |> get(Routes.api_v1_stories_path(conn, :index), %{"page" => "1000000"})
        |> json_response(200)

      assert %{"data" => []} == result
    end

    test "/stories/:id returns an nil value when :id is non existent.", %{conn: conn} do
      result =
        conn
        |> get(Routes.api_v1_stories_path(conn, :show, "1"))
        |> json_response(200)

      assert %{"data" => nil} == result
    end

    test "/stories/:id returns a story when :id exists.", %{conn: conn} do
      result =
        conn
        |> get(Routes.api_v1_stories_path(conn, :show, "123"))
        |> json_response(200)

      assert %{"data" => %{"foo" => 123, "id" => 123}} == result
    end
  end
end
