defmodule Hna.HackerNewsTest do
  use ExUnit.Case
  alias Hna.HackerNews

  describe "function" do
    setup do
      :ets.insert(Hna.HackerNews.ETS, {123, %{"foo" => 123, "id" => 123}})

      {:ok, %{}}
    end

    test "fetch/1 returns nil if id is inexistent." do
      assert is_nil(HackerNews.fetch(1))
    end

    test "fetch/1 returns a story if id exists." do
      assert %{"foo" => 123, "id" => 123} == HackerNews.fetch(123)
    end

    test "interval/2 returns a list of stories." do
      assert is_list(HackerNews.interval(0))
    end

    test "insert/1 inserts a new story if it does not exist." do
      result = HackerNews.insert(%{"foo" => 124, "id" => 124})

      assert {:ok, %{"foo" => 124, "id" => 124}} == result
    end

    test "insert/1 does not insert a new story if it does exist." do
      result = HackerNews.insert(%{"foo" => 123, "id" => 123})

      refute {:ok, %{"foo" => 123, "id" => 123}} == result
    end
  end

  describe "supervisor" do
    test "does start the :ets table correctly." do
      assert Hna.HackerNews.ETS = get_in(:ets.info(Hna.HackerNews.ETS), [:name])
    end
  end
end
